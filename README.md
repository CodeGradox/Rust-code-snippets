# Rust code snippets
Some code I use when learning Rust.

### Read programs arguments
```Rust
for arg in env::args() {
        println!("{}", arg);
}
```

### Read from std
```Rust
use std::io;

fn main() {
    let buffer = match read_stdin() {
        Ok(m) => m,
        Err(_) => panic!("Error"),
    };
    println!("{}", buffer);
}

fn read_stdin() -> Result<String, io::Error> {
    let mut buffer = String::new();
    try!(io::stdin().read_line(&mut buffer));
    Ok(buffer.trim().to_string())
}
```

### Use Box<T>

```Rust
use std::collections::HashMap;
struct Point {
        x: i32,
        y: i32,
}

fn main() {
        let mut map: HashMap<String, Box<Point> = HashMap::new();
        
        for i in 1..5 {
                let key = format!("{} {}", i, i);
                let point = Point{ x: i, y: i };
                map.insert(key, Box::new(point)); // Give map it's own reference to point
        }
}
```
